console.log('Hellowwurld')

//arithmetic operators
	let x= 1397;
	let y= 7831;

	let sum= x + y;
	console.log("Result of addition operator: "+ sum);

	let difference= x - y;
	console.log("Result of subtraction operator: "+ difference);

	let product= x * y;
	console.log("Result of multiplication operator: "+ product);

	let quotient = x / y;
	console.log ("Result of division operator: "+ quotient);

	let remainder = y % x;
	console.log('Result of modulo operator: '+ remainder)

	//another example for remainder/modulo

		let divisor= 2;
		let dividend= 25;

		let modulo= dividend % divisor;
		console.log('Result of Modulo Operator= '+ modulo)

	//assignment operators

		//Basic assignment operator (=)
		let assignmentNumber= 10;

		//addition assignment operator (+=)
		assignmentNumber= assignmentNumber+ 2;
		console.log('Result of addition assignment operator: '+ assignmentNumber);

		//shorthand for assignmentNumber= assignmentNumber+ 2

		assignmentNumber += 2;
		console.log('Result of addition assignment operator: '+ assignmentNumber);

		//subtraction /multiplication/division assignment operator (-=, *=, /=)
		assignmentNumber -= 2;
		console.log('Result of subtraction assignment operator: '+ assignmentNumber);

		assignmentNumber *= 2;
		console.log('Result of multiplication assignment operator: '+ assignmentNumber);

		assignmentNumber /= 2;
		console.log('Result of division assignment operator: '+ assignmentNumber);

	//multiple operators and parentheses

		let mdas= 1 + 2 - 3 * 4 / 5;
		console.log('Result of mdas operation: '+ mdas);

		let pemdas = 1 + (2 - 3) * (4 / 5);
		console.log('Result of pemdas operation: '+ pemdas);


	//increment and decrement
		//operators that add or subtract values by 1

		let z= 1;
		let increment= ++z;

		console.log('Result of pre-increment: '+increment);
		//the value of 'z' was also icnreased even though we didn't implicitly specify any value of reassignment
		console.log('Result of pre-increment: '+ z);

		increment= z++
		console.log('Result of post-increment: '+ increment);
		//the value of 'z' was increased again reassignming the value to 3
		console.log("Result of post-increment: " +z)

	//Pre-increment
		let blankets= 10
		let newBlankets= ++blankets;

		console.log('blankets: ' + blankets);
		console.log('newBlankets: '+ newBlankets);

		//post increment

		let pillows = 10;
		let newPillows= pillows++;
		console.log("pillows: "+ pillows);
		console.log('new pillows: '+ newPillows);

	//pre-decrement
		let soda= 10;
		let newSoda= --soda;
		console.log('soda: '+soda)
		console.log('new soda: '+newSoda);

	//post-decrement
		let nuggets = 10;
		let newNuggets = nuggets--;

		console.log('nuggets: '+nuggets);
		console.log('new nuggets: '+newNuggets);


//type coercion
	/*Type Coercion is the automatic or implicit conversion of values from one data type to another*/


		let numA= '10';
		let numB= 12;

			let coercion =numA + numB;
			console.log(coercion);
			console.log(typeof coercion);

		let numC = 16;
		let numD = 14;

			let nonCoercion = numC + numD;
			console.log(nonCoercion);
			console.log(typeof nonCoercion);


//comparison ooperators
		
		let juan = 'juan';

		//equality operator (==)
		/*
			-checks whetheer the operands are equal/have the same content
			-attempts to convert and compare
			-returns a boolean value
		*/

		console.log(1 == 1);
		console.log(1 == 2);
		console.log(1 == "1");

		//compares two strings that are the same

		console.log('juan' == 'juan');
		//compares a string with the variable 'juan' declared above
		console.log('juan' == juan);


		//inequality operator !=
		/*
			-checks whetehr the operands are not equal/ have differnet content
		*/

		console.log(1 != 1);
		console.log(1 != 2);
		console.log(1 != '1');
		console.log(0 != false);
		console.log('juan' != 'juan');
		console.log('juan' != juan);


		//strict equality operator (===)

		console.log(1 === 1);
		console.log(1 === 2);
		console.log(1 === '1');
		console.log(0 === false);
		console.log('juan' === 'juan')
		console.log('juan' === juan);


		//strict inequality operator (!==)

		console.log(1 !== 1);
		console.log(1 !== 2);
		console.log(1 !== '1');
		console.log(0 !== false);
		console.log('juan' !== 'juan')
		console.log('juan' !== juan);

	//relational operators

		// some comparison operators check
		//whether one value is greater or less than to the other value 
		let a = 50;
		let b = 65;

		let isGreaterThan = a > b;
		let isLessThan = a < b;
		let isGTorEqual = a >= b;
		let isLTorEqual = a <= b;

		console.log(isGreaterThan);
		console.log(isLessThan);
		console.log(isGTorEqual);
		console.log(isLTorEqual);

	//Logical Operators

		let isLegalAge= true;
		let isRegistered= false;

		//Logical AND Operator (&& - Double Ampersand)
		//Returns true if all operands are true

		let allRequirementsMet= isRegistered && isLegalAge
		console.log('Result of logical AND operator: '+ allRequirementsMet);

		//Logical OR Operator (|| - Double Pipe)
		//Returns true if one of the operands are true

		let someRequirementsMet = isLegalAge || isRegistered;
		console.log('Result of Logical OR operator: '+ someRequirementsMet);

		//Logical NOT Operator ( ! - Exclamation Point)

		//Returns the Opposite value
		let someRequirementsNotMet= !isRegistered;
		console.log('Result of Logical NOT operator: '+someRequirementsNotMet);




